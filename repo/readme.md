this subdir is our mvn repo. to use it just add the following to your pom:

```
<repositories>
...
  <repository>
    <id>ohba</id>
    <url>https://bitbucket.org/ohba/autumn/raw/master/repo</url>
  </repository>
...
</repositories>
```

thanks for playing
